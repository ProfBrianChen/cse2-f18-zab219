 
///////Zachery Badrawi
//////9-4-18
//// CSE 02-21
///WelcomeClass
//
public class WelcomeClass{
  
  public static void main(String args[]){
    ///Each line prints specidified text
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    ///Double back slashes needed to avoid syntax errors
    System.out.println("<-Z--A--B--2--1--9->"); 
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
    System.out.println("  v  v  v  v  v  v"); 
  }
}
                       