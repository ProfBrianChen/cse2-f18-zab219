///////Zachery Badrawi
//////9-7-18
//// CSE 02-21
///Arithmetic
//
public class Arithmetic{
  // main method required for Java programs
  public static void main(String args[]){
      //Program is used to calculate total cost of each item, sales tax of each item, total cost of pruchases before tax, total sales tax, and the cost of the final transaction.

//Number of pairs of pants
    int numPants = 3;
//Cost per pair of pants
    double pantsPrice = 34.98;
//Number of sweatshirts
    int numShirts = 2;
//Cost per shirt
    double shirtPrice = 24.99;
//Number of belts
    int numBelts = 1;
//cost per belt
    double beltCost = 33.99;

//the tax rate
    double paSalesTax = 0.06;

    //Costs of each type of product (quantity times individual cost)  
    double totalCostPants=(pantsPrice * numPants);
    double totalCostShirts=(numShirts * shirtPrice);
    double totalCostBelts=(numBelts * beltCost);

    //Print the total cost of each item.
    System.out.println("The total cost for pants is $" + totalCostPants + ", for  shirts is $" + totalCostShirts + ", and for belts is $" + totalCostBelts+ ".");

    
    //Calculations for sales tax of pants
    double salesTaxPants=(totalCostPants * paSalesTax);
    //Calculations for sales tax of shirts
    double salesTaxShirts=(totalCostShirts * paSalesTax);
    //Calculations for sales tax of belts
    double salesTaxBelts=(totalCostBelts * paSalesTax);
    //reducing decimal place for pants tax
    salesTaxPants= Math.round(salesTaxPants *100.0)/100.0;
    //reducing decimal place for shirts tax
    salesTaxShirts= Math.round(salesTaxShirts *100.0)/100.0;
    //reducing decimal place for belts tax
    salesTaxBelts= Math.round(salesTaxBelts *100.0)/100.0;
    
    //Print tax for each item
    System.out.println("The sales tax for the pants is $" + salesTaxPants + ", for the shirts is $" + salesTaxShirts + ", and for the belts is $" + salesTaxBelts+ ".");
    
    
    //Calculation for total cost of all purchases
    double totalCostPurchase=(totalCostBelts+totalCostShirts+totalCostPants);
    //Reducing decimals for total cost value
    totalCostPurchase= Math.round(totalCostPurchase *100.0)/100.0;
    
    //Printing total cost of purchase
    System.out.println("The total cost of the purchase before tax is $" + totalCostPurchase+ ".");
    
    //Calculating total sales tax
    double totalSalesTax=(salesTaxPants+salesTaxBelts+salesTaxShirts);
    //Reducing decimal place for sales tax
    totalSalesTax= Math.round(totalSalesTax *100.0)/100.0;
    
    //Printing total sales tax
    System.out.println("The total sales tax is $" + (totalSalesTax)+ ".");
    
    //Calculating final transaction
    double transaction= (totalCostPurchase+totalSalesTax);
    
    //Print final transaction cost
    System.out.println("The final transaction is $" + (transaction)+ ".");

  }
}