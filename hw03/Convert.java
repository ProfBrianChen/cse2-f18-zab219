///////Zachery Badrawi
//////9-14-18
//// CSE 02-21
///Convert
//
import java.util.Scanner;
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //program requests area in acres, rainfall, and calculates the quantity in cubic miles.
Scanner myScanner = new Scanner( System.in ); //initiate scanner
System.out.print("Enter the affected area in acres: "); // ask for acres value
double acres = myScanner.nextDouble(); //double acres

   System.out.print("Enter the rainfall in the affected area: ");//print request rainfall value
          double rainfall = myScanner.nextDouble(); //double for rainfall value
double cubicMiles= acres*0.0015625*rainfall*0.0000157828; //new double cubic miles value based on acres converted into miles^2 multiplied by rainfall converted into miles
System.out.println("The volume inside the pyramid is: "+ cubicMiles + " cubic miles"); //print cubic miles results

  }
}