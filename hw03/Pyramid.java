///////Zachery Badrawi
//////9-14-18
//// CSE 02-21
///Pyramid
//
import java.util.Scanner;
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //program requests length of sides and height of pyramid to output the volume
Scanner myScanner = new Scanner( System.in ); //initiate scanner
System.out.print("The square side of the pyramid is (input length): ");// request value for pyramid side length
double length = myScanner.nextDouble(); //double for length value

   System.out.print("The height of the pyramid is (input height): ");//print request for pyramid height
          double height = myScanner.nextDouble(); //double for height value
double volume = length*length*height/3; // new double value is equal to length times height times length, all divided by 3
System.out.println("The volume inside the pyramid is: "+ volume); //print volume
  }
}