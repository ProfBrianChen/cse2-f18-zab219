////Zachery Badrawi
////CSE02-21
///9-12-18
//lab03

import java.util.Scanner; //initiate scanner function
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //program requests, check cost, number of people, and tip value, then outputs individual cost of each person.

          Scanner myScanner = new Scanner( System.in ); //initiate scanner
          System.out.print("Enter the original cost of the check in the form of xx.xx:  "); //print request for check cost
          double checkCost = myScanner.nextDouble(); //double for checking cost using scanner
          System.out.print("Enter the precentage tip that you wish to pay as a whole number (in the form xx): ");//print request for tip
          double tipPercent = myScanner.nextDouble(); //double for tip percent from scanner
          tipPercent /=100; //We want to convert thepercentage into decimal value
          
            System.out.print("Enter the number of people who went out to dinner:  "); //System print number people
          int numPeople = myScanner.nextInt(); //int for number of people paying
          
          double totalCost; // double variable of total cost
          double costPerPerson; //double variable of cost per individual
          int dollars,   //whole dollar amoutn of cost 
            dimes, pennies; // for storing digits
                    //to the right of the decimal point
                    //for the cost$
          totalCost = checkCost * (1+tipPercent); //total cost equals the check cost multiplied by 1+tiper percentage
          costPerPerson = totalCost/numPeople;
          //get the whole amoutn, dropping decimal fraction
          dollars=(int)costPerPerson; //dollars are equal to new cost per person value
          //get dimes amoutn, e.g.,
          // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
          //  where the % (mod) operator returns the remainder
          //  after the division:   583%100 -> 83, 27%5 -> 2 
          dimes=(int) (costPerPerson * 10) % 10;  //dimes based on per person
          pennies=(int) (costPerPerson * 100) % 10; //pennies based on per person
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //print total dollars, dimes, and pennies
         
          
}  //end of main method   
  	} //end of class



