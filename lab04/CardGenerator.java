////Zachery Badrawi
////CSE02-21
///9-19-18
//lab04


public class CardGenerator{
    			// main method required for every Java program
   			public static void main(String[] args) {
          //program selects a random card from a deck of 52.
          
    int number = (int)(Math.random()*52)+1; //generate a random int number

    
    String suit="suit"; //initialize suit as a string function
    String card="card"; //initialize card as a string function
          
     
    if ((number >=1) && (number<14)){ //if number is greater than or equal to 1 and less than 14...
                suit="Diamonds";//print "Diamonds"
    }
    else if ((number >=14) && (number<27)){ //if number is greater than or equal to 14 and less than 27...
                suit="Clubs";//print "Clubs"
    }                         
    else if ((number >=27) && (number<40)){ //if number is greater than or equal to 27 and less than 40...
                suit="Hearts";//print "Hearts"
    }
    else if((number >=40) && (number<53)) { //if number is greater than or equal to 40 and less than 53...
                suit="Spades";//print "Spades"
    }        
      
     number=number % 13; //new number is number modulus 13 to find remainder.
          
     switch (number) { //switch function
               case 1: //if number is 1, card becomes an ace
                  card="Ace";    
                  break;

               case 11: //if number is 11, card becomes a joker
                  card="Joker";
                  break;

               case 0: //if number is 0, card becomes a king
                  card="King";
                  break;

               case 12: //if number is 12, card becomes a queen
                  card="Queen";
                  break;

               case 2: //if number is 2, card becomes 2
                  card="2";
                  break;
         
               case 3: //if number is 3, card becomes 3
                  card="3";
                  break;
         
               case 4: //if number is 4, card becomes 4
                  card="4";
                  break;
         
               case 5: //if number is 5, card becomes 5
                  card="5";
                  break;
         
               case 6: //if number is 6, card becomes 6
                  card="6";
                  break;
         
               case 7: //if number is 7, card becomes 7
                  card="7";
                  break;
         
               case 8: //if number is 8, card becomes 8
                  card="8";
                  break;
         
               case 9: //if number is 9, card becomes 9
                  card="9";
                  break;
         
               case 10: //if number is 10, card becomes 10
                  card="10";
                  break; 
           }   
    System.out.println("You chose " + card + " of " + suit); //print "You chose [card number]...of...[suit]
  } 
}