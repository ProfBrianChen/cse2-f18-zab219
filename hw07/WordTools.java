//Zachery Badrwai
//CSE 02
//HW07


import java.util.Scanner;//import scanner function

public class WordTools{//class
  public static void main(String [] args){//main method
    Scanner input = new Scanner(System.in);//initialize new scanner
    
    System.out.println("Enter a sample text:");//request sample text
    String text = input.nextLine();//next input is text
    System.out.println("You entered:"+text);//state text entered
    System.out.println(" ");//skip a line
    
    String choice;//initialize string choice
    
    do{//do...
      
      printMenu();//print menu method
      choice = input.next();//choice is next input
      input.nextLine(); //clear buffer
      switch (choice){
        case "c": //when choice is c
          int number=getNumOfNonWSCharacters(text);//number is number of non white characters from method
          System.out.println("Number of non-whitespace characters: "+number); //print statement for number
          break;
        case "w"://when choice is w
          int wordsCount=getNumOfWords(text);//reference method
          System.out.println("Number of words: "+wordsCount);//print number of words and count
          break;
        case "f": System.out.println("Enter the word or phrase to be found:");//print word request to be found
          String find = input.next();//next input is string to find
          int result = findText(text, find);//method returns results based off of two strings
          System.out.println("'"+find+"' instances: "+result);//print original search term and instances
          break;
        case "r": //when choice is r
          String out=replaceExclamation(text);//out is equal to the exclamation replacement method
          System.out.println(out);//print out
          break;
        case "s": //when choice is s
          String output=shortenSpace(text);//shorten space of words method
          System.out.println(output);//print new text
          break;
        case "q": //when choice is q
          System.out.println("Goodbye");//print out goodbye message
          break;
        default: System.out.println("you entered an invalid value -- try again");//print error and ask for new entry
        break;
      }
    }while(!(choice.equals("q")));//while choice does not equal q
  }
  public static void printMenu(){//print menu method (print selection of options)
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println(" ");
    System.out.println("Choose an option:");
  }
  
  public static int findText(String input, String searchWord) {//int method to find count of word in text string
    int searchCount = 0;//search count is 0
    int c =0;// c count is 0
    int jCount = 0;//jCount is 0
    boolean inWord = false;//false circumstance
    for (int i = 0; i < input.length(); i++) {//iterate through characters
      if (input.charAt(i) == searchWord.charAt(0)) {//check if character aligns with search term
        for (int j = 1; j < searchWord.length(); j++) {//check if full word length matches
          c = 0;
          if (input.charAt(i+j) != searchWord.charAt(j)){//if word not found
            c++;//add iteration
          }
          jCount = j;//character count= j
        }
        if (c == 0) { //if so
          searchCount++;//instance count increases by 1
        }
        else { //otherwise
          c = 0;// c is reset to 0
        }
        i = i+jCount;//new iteration is iteration plus character count
      }
    }
    return searchCount;//return is value of count based on instances
  }
  
  public static String replaceExclamation(String input){//String method using text as input
    input=input.replace("!",".");//return
    return input;//return the input
  }
  public static String shortenSpace(String input){//string method using text as input
    input=input.replaceAll("  "," ");//replace all double spaces with single space
    return input;//return the input
  } 
  private static int getNumOfWords(String input) {//int method using text as input
    String[] numWords=input.split(" "); // .split will split the components of the string into individual components
    return numWords.length;//.length returns the number of string components
  }
  public static int getNumOfNonWSCharacters(String input){ //int method using text as input
    input=input.replaceAll(" ","");//replace all spaces with nothing
    return input.length(); //.length function finds length of string
  }
}
