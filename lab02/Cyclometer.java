////Zachery Badrawi
////CSE02-21
///9-5-18
//lab02

  
  public class Cyclometer {
    // main method required for Java programs
    public static void main(String[] args) {
      //Program is used to calculate distance, rotation count, and time of 2 trips recorded by a bicycle cyclometer
      
      
      //Input data
      
      int secsTrip1=480; //integer for seconds of trip 1
      int secsTrip2=3220; //integer for seconds of trip 2
      int countsTrip1=1561; //integer for the counts of trip 1
      int countsTrip2=9037; //integer for the counts of trip 2
      double wheelDiameter=27.0, //decimel value of wheel diameter
      PI=3.1459,  //value of pi
      feetPerMile=5280, //feet in a mile
      inchesPerFoot=12, //inches in a foot
      secondsPerMinute=60; //seconds in a minute
      double distanceTrip1, distanceTrip2,totalDistance; //the distance of trip 1, trip 2, and combined are a decimel value
      
      // print data for time and rotation cycles for each trip
      System.out.println("Trip 1 took "+
                        (secsTrip1/secondsPerMinute)+" minutes and had "+
                        countsTrip1+" counts.");
      System.out.println("Trip 2 took "+
                        (secsTrip2/secondsPerMinute)+" minutes and had "+
                         countsTrip2+" counts.");
      // Calculations
      distanceTrip1=countsTrip1*wheelDiameter*PI; //distance calculation for trip 1
      // above gives distance in inches
      // (for each count, a rotation of the wheel travels)
      // the diaemeter in inches time Pi
      distanceTrip1/=inchesPerFoot*feetPerMile; //conversion calculation for trip 1
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance calculation and conversion for trip 2
      totalDistance=distanceTrip1+distanceTrip2; //total distance is combination of trip 1 and 2
      
      
      // print out end results for distance traveled in trip 1, trip 2, and their combined value
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //end results for trip 1
      System.out.println("Trip 2 was "+distanceTrip2+" miles"); //end results for trip
      System.out.println("The total distance was "+totalDistance+" miles"); // results for total distance travelled

    } //end main
  } //end of class