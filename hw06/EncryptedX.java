////Zachery Badrawi
////CSE02-21
///10-10-18
//HW06

import java.util.Scanner; //initiate scanner function
public class EncryptedX{
   // main method required for every Java program
      public static void main(String[] args) { //program asks for integer value and then proceeds to output relevant X pattern
        Scanner sc = new Scanner( System.in ); //initiate scanner
      
        int num_x=0;//initiate scanner number x as integer 0
        
        boolean correctInputx = false;//input is false for boolean value
        
        while (!correctInputx){//while correct input is not true
          System.out.println("Please enter integer value from 1 to 100");//print request for integer value
          
          if (sc.hasNextInt()){//if true scanner moves to next line
            num_x=sc.nextInt();//scan next int
            correctInputx= true;//correct input becomes true
            if(num_x<1||num_x>100){//if num is between 1 and 10
              sc.nextLine();//move to next scanner line
              System.out.println("Error: Please enter a valid integer.");//print error message
              correctInputx= false;//correct input becomes true
              
            }
          }
          else{// when if statment is not true
            sc.nextLine();//move to next scanner line
            System.out.println("Error: Please enter a valid integer.");//print error message and ask for new integer 
          }
        }
        
        int num_y=num_x;// y integer is equal to x integer
        
        
        
        for(int numRows = 1; numRows<=num_y/2; numRows++){ //number of rows is equal to integer input for half of the pattern
          for (int numLinesA=1;numLinesA <= numRows-1; numLinesA++){//add stars for the left side of pattern
            
            System.out.print("*");//print space
          }
          System.out.print(" ");//print space
          for (int numLinesB=1;numLinesB <= num_x-numRows*2; numLinesB++){//add stars for the center group in pattern
            
            System.out.print("*");//print star
          }
          System.out.print(" ");//print space
          for (int numLinesC=1;numLinesC <= numRows-1; numLinesC++){//add stars for the right side of pattern
            
            System.out.print("*");//print star
          }
          
          System.out.println();//move to next print line
        }
        
         for(int numRows2 = num_y/2; numRows2>=1; numRows2--){ //number of rows is equal to integer input and decreases until it reaches 1
          for (int numLinesA2=1;numLinesA2 <= numRows2-1; numLinesA2++){//number of lines starts at 1 and increases until it reaches row number
            
            System.out.print("*");//print star
          }
          System.out.print(" ");//print space
          for (int numLinesB2=1;numLinesB2 <= num_x-numRows2*2; numLinesB2++){//number of lines starts at 1 and increases until it reaches row number
            
            System.out.print("*");//print star
          }
          System.out.print(" ");//print space
          for (int numLinesC2=1;numLinesC2 <= numRows2-1; numLinesC2++){//number of lines starts at 1 and increases until it reaches row number minus 1
            
            System.out.print("*");//print star
          }
          
          System.out.println();//move to next print line
        }
        
      }           
}