////Zachery Badrawi
////CSE02-21
///10-10-18
//HW09

import java.util.Scanner; //initiate scanner function
public class HW10{//HW 10
  public static void main(String[] args) {//main method
    Scanner myScanner = new Scanner(System.in);//scanner
    int[][] arrayNums= {{1, 2, 3},{4, 5, 6},{7, 8, 9}};//numbered array
    String[][] arrayString={{"1", "2", "3"},{"4", "5", "6"},{"7", "8", "9"}};//string array of values to be displayed
    boolean end=false;//boolean starts false
    int a,b;//int a and b
    for (a=0;a<arrayString.length;a++){//for a starting at 0 and less than array size, iterate
      System.out.println();//new line
      for (b=0;b<arrayString.length;b++){//iterating to print array
        System.out.print(arrayString[a][b]+" ");//print array
      }
    }
    while (end==false){//while boolean is false
      int count;//int count
      for (count=1;count<100;count++){//for count is less than 100 (number doesnt matter because max player moves cant reach 100.
        if (count%2==1){//if count mod 2.
          System.out.println();//print new line
          System.out.println();//print new line
          System.out.println("Player1, please enter a value to replace with X.");//print player 1 prompt
          while(myScanner.hasNextInt()==false){//if scanner takes in incorrect data type
            System.out.println("You entered a non integer -- try again.");//print statement
            myScanner.nextLine();//clear buffer
          }
          int x=myScanner.nextInt();//x is next scanner value
          while(x<1||x>9){//while x is less than 1 or greater than 9
            System.out.println("You entered an integer that is out of range -- try again.");//print statement 
            x=myScanner.nextInt();//clear buffer
          } 
         
          arrayString=player1(arrayNums,x,arrayString);//player method is equal to array string
          for (a=0;a<arrayString.length;a++){//for iterating string elements
            System.out.println();//new line
            for (b=0;b<arrayString.length;b++){//colum iteration
              System.out.print(arrayString[a][b]+" ");//print array
            }
          }
          int player=1;//player value 1
          end=victory(arrayString,"X", player, end);//victory method
          if (end==true){//if end is true now...
            break;//break
          }
        }
        
        else{//else
          System.out.println();//print new line
          System.out.println("Player2, please enter a value to replace with O.");//print player 2 prompt
          while(myScanner.hasNextInt()==false){//while next input is not int
            System.out.println("You entered a non integer -- try again.");//print error
            myScanner.nextLine();//clear buffer
          }
          int o=myScanner.nextInt();//scan for next int
          while(o<1||o>9){//while o is less than 1 or greater than 9
            System.out.println("You entered an integer that is out of range -- try again.");//error statement
            o=myScanner.nextInt();//clear buffer
          } 
          arrayString=player2(arrayNums,o,arrayString);//player 2 method
          for (a=0;a<arrayString.length;a++){//a iterated as row
            System.out.println();//new line
            for (b=0;b<arrayString.length;b++){//b iterated as column
              System.out.print(arrayString[a][b]+" ");//print array
            }
          }
          int player=2;// player number is 2
          end=victory(arrayString,"O", player, end);//victory method
          if (end==true){//if end is true
          break;//break loop
          }
        }
      }
    }
    
  }
  public static String[][] player1(int[][] arrayNums, int key, String[][] arrayString){//method for player 1
    String [][] arrayNew= new String [3][3];//new array 3 by 3
    Scanner myScanner = new Scanner(System.in);//scanner
    int i,j;//int i and j
    for (i=0;i<arrayNums.length;i++){//i iteration for rows
      for(j=0;j<arrayNums.length;j++){//j iterations for columns
        while(arrayNums[i][j]==key && (arrayString[i][j]=="X" || arrayString[i][j]=="O")) {//while elements are taken up by x or o
          System.out.println("Position already belongs to X or O. Please pick a new location. ");//print error
          key=myScanner.nextInt();//clear buffer
        }
        if (arrayNums[i][j]!=key){//if array is not equal to key
          arrayNew[i][j]=arrayString [i][j]; //array carries elements from string array
        }
        if(arrayNums[i][j]==key){//if array equal to key
          arrayNew[i][j]="X";//array element is X
        }
      }
    }
    return arrayNew;//return array
  }
  
  public static String[][] player2(int[][] arrayNums, int key, String[][] arrayString){//method player 2
    String [][] arrayNew= new String [3][3];//new string is 3 by 3
    Scanner myScanner = new Scanner(System.in);//scanner initiated
    int i,j;//int i and j
    for (i=0;i<arrayNums.length;i++){//i array iteration for row
      for(j=0;j<arrayNums.length;j++){//j array iteration for column
         while(arrayNums[i][j]==key && (arrayString[i][j]=="X" || arrayString[i][j]=="O")) {//while array element is taken up by o or x
          System.out.println("Position already belongs to X or O. Please pick a new location. ");//print error
          key=myScanner.nextInt();//clear buffer
        }
        if (arrayNums[i][j]!=key){//if array element is not equal to key
          arrayNew[i][j]=arrayString [i][j];  //new array element is equal to string array
        }
        if(arrayNums[i][j]==key){//if numbered array matches key value
          arrayNew[i][j]="O";//key is now O
        }
      }
    }
    return arrayNew;//return new array layout
  }
  
  public static boolean victory(String[][] array, String key, int count, boolean end){//method for ending loops
    for (int i=0; i<array.length;i++){//for row iteration
      for(int j=0; j<array.length;j++){//for column iteration
        if (array[i][0]==key && array[i][1]==key && array[i][2]==key||
            array[0][j]==key && array[1][j]==key && array[2][j]==key||
            array[0][0]==key && array[1][1]==key && array[2][2]==key||
            array[0][2]==key && array[1][1]==key && array[2][0]==key)
        {// if winning array layout occurs..
          
          end=true;//end boolean now true
        }
        
      }
    }
    if (end==true){//if end is true
    System.out.println();//print new line
    System.out.println();//print new line
    System.out.println("Player"+count+" won!");// print player who won
    }
   return end;//end
  } 
}
  